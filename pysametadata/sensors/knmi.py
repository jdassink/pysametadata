# -*- coding: utf-8 -*-
"""
KNMI sensor classes

.. module:: knmi

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2016-2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
import numpy as np
from pysametadata.sensors.sensor import Sensor


class KNMImb(Sensor):
    def __init__(self, channel, sens_freq=1.0, serial_number=None,
                 wnrs_diameter=False, **kwargs):
        self.channel = channel
        Sensor.__init__(self)

        self.model = kwargs['model']
        self.type = 'KNMI-mb {}'.format(self.model)
        self.manufacturer = 'KNMI'
        self.serial_number = serial_number
        self.description = '{} {} differential microbarometer'.format(
            self.manufacturer, self.model
            )

        # Original gain estimate is from:
        # Evers and Haak (2000), The Deelen Infrasound Array ..., KNMI-TR225
        # Factor has been adjusted to match Hyperion IFS-5000
        self.gain = 0.655/37.0 * 1.45   # V / Pa
        self.sens_freq = sens_freq      # Hz
        self.wnrs_diameter = wnrs_diameter
        self.mode = 'filtered'
        self.set_equipment()
        return

    def get_paz(self):
        # KNMImb design parameters ------------------------------------------
        V_1 = 2.20E-5             # Fore volume               [ m^3           ]
        V_2 = 1.03E-4             # Reference volume          [ m^3           ]
        l_inl = 60.0E-3           # Inlet length              [ m             ]
        r_inl = 5.00E-3           # Inlet radius              [ m             ]
        C_d = 7.5E-11             # Diaphragm sensitivity     [ m^4 s^2 kg^-1 ]

        # Standard atmospheric conditions -----------------------------------
        # T     = 20.0            # Temperature               [ C             ]
        P_A = 1.01E05             # Ambient pressure          [ Pa            ]
        gamma = 1.40              # Ratio of specific heats   [ --            ]
        mu = 1.82E-5              # Dynamic viscosity         [ kg m^-1 s^-1  ]

        # rho = 1.21              # Ambient density           [ kg m^-3       ]
        # rho_1 = 6.33E06         # Porous hose resistance    [ kg m^-4 s^-1  ]
        # rho_2 = 4.63E10         # Capillary resistance      [ kg m^-4 s^-1  ]
        # a = 2.12E-5             # Thermal diffusivity       [ m^2 s^-1      ]
        # c_snd = 340.0           # Speed of sound            [ m s^-1        ]
        # -------------------------------------------------------------------

        if self.model == '500s':
            # Original 500s version, see Evers, PhD thesis, 2008
            # or Mentink and Evers, JASA, 2011
            # 10 cm length, 0.1 mm radius
            l_cap = 10.0 / 1e2        # Capillary length   [ m ]
            r_cap = 0.1 / 1e3        # Capillary radius   [ m ]
        elif self.model == '1000s':
            # 5.0 cm length, 0.1 mm diameter
            l_cap = 5.0 / 1e2         # Capillary length   [ m ]
            r_cap = (0.1 / 1e3) / 2   # Capillary radius   [ m ]
        else:
            raise ValueError('KNMI-mb type {} doesnt exist'.format(self.model))

        # ---------------------------------------------------------------------
        # Acoustic resistance values [ kg m^-4 s^-1  ]
        # R_2, low-frequency cut-off is determined by the resistance of the
        # capillary (Eqn. 7, Mentink and Evers)
        R_2 = 8*mu*l_cap/(np.pi*np.power(r_cap, 4))

        # R_1, the high-frequency cut-off is determined by the KNMImb
        # inlet parameters:
        R_1 = 8*mu*l_inl/(np.pi*np.power(r_inl, 4))
        # R_1   = R_2 * 1e-4;   # Mentink and Evers, JASA 2011

        # Alternatively, R_1 can include the resisitivity due to the porous
        # hose this is not preferable; it is better to include another filter
        # stage in the dataless response

        # R_1   = 3.8E07  # Haak en de Wilde, KNMI TR, 1996  ... porous hose
        # ---------------------------------------------------------------------

        C_1 = V_1/(gamma*P_A)
        C_2 = V_2/(gamma*P_A)

        tau_1 = R_1*C_1
        tau_2 = R_2*C_2

        A = 1 + tau_1/tau_2 + R_1/R_2 + C_d/C_2
        B = 1 + C_d*(1./C_1 + 1./C_2)

        term_1 = -A/(2*tau_1*B)
        term_2 = 1.0/(2*tau_1*B)
        term_3 = np.sqrt(np.power(1 - tau_1/tau_2 - R_1/R_2 + C_d/C_2, 2) +
                         4*R_1/R_2)

        s_1 = term_1 + term_2 * term_3
        s_2 = term_1 - term_2 * term_3

        if self.wnrs_diameter:
            # Replace largest pole by WNRS pole
            s_D = self.compute_wnrs_pole()
            s_2 = s_D

        poles = [s_1, s_2]
        zeros = [0.0]

        return (poles, zeros)
