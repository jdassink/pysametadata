# -*- coding: utf-8 -*-
"""
Commissariat a l'Energie Atomique (CEA) sensor classes

.. module:: cea

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2016-2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
from pysametadata.sensors.sensor import Sensor


class CEA(Sensor):
    def __init__(self):
        Sensor.__init__(self)

        self.type = '{}'.format(self.model)
        self.manufacturer = 'CEA-DASE'
        self.description = '{} {} absolute microbarometer'.format(
            self.manufacturer, self.model
            )

        return


class MB200x(CEA):
    def __init__(self, channel, wnrs_diameter=False, mode='filtered',
                 **kwargs):
        self.model = kwargs['model']
        self.mode = mode
        self.channel = channel
        CEA.__init__(self)

        if self.mode == 'filtered':
            self.gain = 20.0e-3     # V / Pa
        elif self.mode == 'unfiltered':
            self.gain = 1.0e-3      # V / Pa

        self.sens_freq = 1.0        # Hz
        self.wnrs_diameter = wnrs_diameter
        self.set_equipment()
        return

    def get_paz(self):
        # Corresponds values in the Nominal Response Library (NRL)
        s_1 = -177.7 - 177.7j
        s_2 = -177.7 + 177.7j
        s_3 = -0.06280
        s_4 = -207.345
        poles = [s_1, s_2, s_3, s_4]
        zeros = [0.0]

        return (poles, zeros)


class MB3a(CEA):
    def __init__(self, channel, wnrs_diameter=False, mode='filtered',
                 **kwargs):
        self.model = 'MB3a'
        self.mode = mode
        self.channel = channel
        CEA.__init__(self, **kwargs)

        if self.mode == 'filtered':
            self.gain = 20.0e-3     # V / Pa
        elif self.mode == 'unfiltered':
            self.gain = 1.0e-3      # V / Pa

        self.sens_freq = 0.25        # Hz
        self.wnrs_diameter = wnrs_diameter
        self.set_equipment()
        return

    def get_paz(self):
        # Corresponds to values in Appendix A of Sandia Report SAND2014-20108
        s_1 = -0.062834
        s_2 = -156.25
        s_3 = -142.122 + 706.193j
        s_4 = -142.122 - 706.193j
        poles = [s_1, s_2, s_3, s_4]

        z_1 = 0.0
        z_2 = -1156.25
        zeros = [z_1, z_2]

        return (poles, zeros)
