# -*- coding: utf-8 -*-
"""
Sensor classes

.. module:: sensor

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2016-2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
import numpy as np

from obspy.core.inventory.response import (PolesZerosResponseStage,
                                           ResponseStage)
from obspy.core.util.obspy_types import ComplexWithUncertainties
from obspy.core.inventory.util import Equipment, Frequency
from obspy.signal.invsim import paz_to_freq_resp


class Sensor(object):
    def __init__(self):
        self.resource_id = self.channel.resource_id
        return

    def compute_norm_factor(self):
        """
        Compute normalization factor A0
        """
        srate = self.channel.sample_rate
        nfft = int(srate * 100)
        gain_factor = 1.0

        # Convert to Obspy complex type
        for ip in range(0, len(self.poles)):
            self.poles[ip] = ComplexWithUncertainties(self.poles[ip])
        for iz in range(0, len(self.zeros)):
            self.zeros[iz] = ComplexWithUncertainties(self.zeros[iz])

        # Compute transfer function
        (H, f) = paz_to_freq_resp(
            self.poles, self.zeros, gain_factor, 1./srate, nfft, freq=True
            )
        try:
            f_idx = np.where(f == self.sens_freq)[0][0]
            a0 = 1.0 / np.abs(H[f_idx])
        except Exception as e:
            print('Normalization factor A0 could not be '
                  'at frequency %f' % self.sens_freq)
            print(e)
        return a0

    def compute_wnrs_pole(self):
        """
        In case a wind noise reduction system (WNRS) is attached to a
        microbarometer, a low-pass filter is effectively introduced.
        The WNRS acts as a low-pass filter for frequencies, for which
        the wavelengths are smaller than twice the diameter D of the
        WNRS system (see Mentink & Evers (2011), JASA, page 37).

        The cut-off frequency (in rad/s) can be computed as:
        omega_D = 2*pi*f = 2 * pi * c / (2 * D) = pi*c / D

        Ideally, the filter would be represented as a separate stage in the
        Metadata file, as a first stage (PA->PA) that precedes the sensor
        stage (PA->V).

        To be compatible with current versions of the SC3ML scheme, the filter
        is taken into account by replacing the largest negative sensor pole
        (which describes the high-frequency behaviour) by the WNRS pole.
        """
        c_snd = 340.0
        omega_D = np.pi*c_snd / (10*self.wnrs_diameter)
        # Convert to Laplace domain:
        s_D = -1.0*omega_D
        return s_D

    def set_equipment(self):
        self.channel.sensor = Equipment(
            type=self.type,
            description=self.description,
            manufacturer=self.manufacturer,
            model=self.model,
            serial_number=self.serial_number,
            installation_date=self.channel.start_date,
            removal_date=self.channel.end_date,
            calibration_dates=[self.channel.start_date],
            resource_id='Sensor#{}'.format(self.resource_id)
        )

    def set_response(self, **kwargs):
        stage = kwargs['stage']
        in_unit = self.channel.calibration_units

        if self.mode == 'filtered':
            (self.poles, self.zeros) = self.get_paz()
            self.a0 = self.compute_norm_factor()
            self.resp_stage = PolesZerosResponseStage(
                    stage, self.gain, self.sens_freq,
                    in_unit, 'V',
                    'LAPLACE (RADIANS/SECOND)',
                    Frequency(self.sens_freq),
                    self.zeros, self.poles,
                    normalization_factor=self.a0,
                    resource_id2='ResponsePAZ#{}'.format(self.resource_id),
                    input_units_description=in_unit,
                    output_units_description='Volts'
                )
        elif self.mode == 'unfiltered':
            self.resp_stage = ResponseStage(
                    stage, self.gain, self.sens_freq,
                    in_unit, 'V',
                    resource_id2='ResponsePAZ#{}'.format(self.resource_id),
                    input_units_description=in_unit,
                    output_units_description='Volts'
                )

        self.channel.response.response_stages.insert(stage-1, self.resp_stage)
        return


class Dummy(Sensor):
    def __init__(self, channel, model_type='Unknown',
                 model=None, description=None, manufacturer=None,
                 serial_number=None, **kwargs):
        self.channel = channel
        Sensor.__init__(self)

        self.type = model_type
        self.serial_number = serial_number
        self.model = model
        self.description = description,
        self.manufacturer = manufacturer

        self.gain = kwargs['gain']
        self.sens_freq = kwargs['sens_freq']
        self.mode = 'unfiltered'
        self.set_equipment()
        return

    def get_paz(self):
        poles = []
        zeros = []
        return (poles, zeros)
