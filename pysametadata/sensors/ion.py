# -*- coding: utf-8 -*-
"""
ION (iongeo.com) / Sensor B.V. sensor classes

.. module:: ion

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2016-2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
import numpy as np
from pysametadata.sensors.sensor import Sensor


class SM6(Sensor):
    def __init__(self, channel, serial_number=None, **kwargs):
        self.channel = channel
        Sensor.__init__(self)

        self.model = kwargs['model']
        self.type = self.model
        self.serial_number = serial_number
        self.manufacturer = 'ION (iongeo.com)'
        self.description = (f'{self.manufacturer} '
                            f'{self.model} geophone')

        if self.model == 'SM-6 B 4.5 Hz 375 Ohm':
            self.gain = 28.8               # [ V/m/s ]
            self.sens_freq = 10.0          # [ Hz ]
        else:
            raise ValueError('Geophone model {} not known'.format(self.model))

        self.mode = 'filtered'
        self.set_equipment()
        return

    def get_paz(self):
        """
        Computation of the response function of a magnetic coil geophone
        following the model discussed in Section 12.3.2 of
        Aki and Richards (2002) and using the equations published on:
        http://ds.iris.edu/NRL/sensors/hgs/passive_responses.html
        """
        if self.model == 'SM-6 B 4.5 Hz 375 Ohm':
            # SM6 design parameters -------------------------------------------
            bo = 0.56          # Open circuit damping due to mech. properties
            Rs = 8660.0        # Shunt resistor                         [ Ohm ]
            Zamp = 2.0e6       # Est. input impedance of the datalogger [ Ohm ]
            Rc = 375.0         # Feedback coil resistance               [ Ohm ]
            fo = 4.5           # Natural frequency                      [ Hz  ]
            m = 11.1e-3        # Mass                                   [ kg  ]

        # Parallel sum of Rs and Zamp
        if Rs == 0.0:
            Rload = Zamp
        elif Zamp == 0.0:
            Rload = Rs
        else:
            Rload = (Rs * Zamp) / (Rs + Zamp)

        # Total resistance = Rc + Rload
        Rt = Rc + Rload

        omega = 2*np.pi*fo
        # intrinsic sensitivity
        Go = self.gain
        # Update gain to effective sensitivity
        self.gain = Go*(Rload/Rt)
        # Compute damping factors (Equations 12.49 and 12.51)
        bc = np.power(Go, 2)/(2*omega*m*Rt)
        bt = bo + bc
        # Compute RtBcFn factor [ Ohm.Hz ]
        self.RtBcFn = Rt*bc*fo
        self.total_damping = bt
        self.intrinsic_sensitivity = Go
        self.effective_sensitivity = self.gain

        # Simple poles and zeros of response function (Equation 12.3)
        s_1 = (-omega*bt) + 1j*(omega*np.sqrt(1-np.power(bt, 2)))
        s_2 = np.conj(s_1)
        z_1 = 0.0
        z_2 = 0.0
        poles = [s_1, s_2]
        zeros = [z_1, z_2]

        return (poles, zeros)
