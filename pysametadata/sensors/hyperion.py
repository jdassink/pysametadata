# -*- coding: utf-8 -*-
"""
Hyperion Technology Group, Inc. sensor classes

.. module:: hyperion

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2016-2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
import numpy as np
import re
from pysametadata.sensors.sensor import Sensor

IFS_SERIES = ['IFS-3000',
              'IFS-5000',
              'IFSN-5102-100',
              'IFS-5200']

sensitivities = {}  # mV / Pa
sensitivities['20121010.001'] = 137.600
sensitivities['20160129.001'] = 139.210
sensitivities['2019xxxx.001'] = 139.337
sensitivities['20210203.001'] = 139.897
sensitivities['20210203.002'] = 138.425
sensitivities['20210203.003'] = 138.867
sensitivities['20210203.004'] = 138.097
sensitivities['20201215.005'] = 139.051
sensitivities['20210203.006'] = 139.804
sensitivities['20210203.007'] = 139.540
sensitivities['20210203.008'] = 139.831


class IFS3000(Sensor):
    def __init__(self, channel, gain=140.0, sens_freq=0.5, wnrs_diameter=False,
                 model_type='IFS-3000', serial_number=None, **kwargs):
        self.channel = channel
        Sensor.__init__(self)

        self.model = 'IFS-3000'
        self.type = model_type
        self.serial_number = serial_number
        self.manufacturer = 'Hyperion Technology Group, Ltd.'
        self.description = (f'{self.manufacturer} '
                            f'{self.model} acoustic sensor')

        if serial_number:
            gain = sensitivities[serial_number] / 1e3
        else:
            gain /= 1e3

        # Infrasound channel sensitivity in V / Pa
        self.gain = gain

        self.sens_freq = sens_freq  # Hz
        self.wnrs_diameter = wnrs_diameter
        self.mode = 'filtered'
        self.set_equipment()
        return

    def get_paz(self):
        if self.type in IFS_SERIES:
            f_1 = 1.483 / 1e3
            f_2 = 3.387 / 1e3
            f_3 = 29.49 / 1e3
        elif self.type == 'NCPA':
            f_1 = 0.00137563
            f_2 = 0.00266932
            f_3 = 0.0201064
        elif self.type == 'CTBTO-IMS':
            f_1 = 0.000742936
            f_2 = 0.00169348
            f_3 = 0.014744

        # Convert to Laplace domain
        s_1 = -2*np.pi*f_1
        s_2 = -2*np.pi*f_2
        s_3 = -2*np.pi*f_3
        poles = [s_1, s_2, s_3]

        z_1 = 0.0
        z_2 = 0.0
        z_3 = 0.0
        zeros = [z_1, z_2, z_3]

        if self.wnrs_diameter:
            # Add pole if WNRS is attached
            s_D = self.compute_wnrs_pole()
            poles.append(s_D)

        return (poles, zeros)


class IFS5000(Sensor):
    def __init__(self, channel, gain=140.0, sens_freq=0.5, wnrs_diameter=False,
                 model_type='IFS-5000', serial_number=None, **kwargs):
        self.channel = channel
        Sensor.__init__(self)

        self.model = 'IFS-5000'
        self.type = model_type
        self.serial_number = serial_number
        self.manufacturer = 'Hyperion Technology Group, Ltd.'
        self.description = (f'{self.manufacturer} '
                            f'{self.model} acoustic sensor')

        if serial_number:
            gain = sensitivities[serial_number] / 1e3
        else:
            gain /= 1e3

        if re.match(r'.DF\b', channel.code):
            # Infrasound channel sensitivity in V / Pa
            self.gain = gain
        elif re.match(r'.GZ\b', channel.code):
            # Acceleration channel sensitivity in V / (m/s**2)
            # relates to pressure gain by factor ~10.46 Pa / (m/s**2)
            self.gain = gain * (1.465/0.140)
        else:
            raise ValueError('Channel type should end in "DF" or "GZ"')

        self.sens_freq = sens_freq  # Hz
        self.wnrs_diameter = wnrs_diameter
        self.mode = 'filtered'
        self.set_equipment()
        return

    def get_paz(self):
        if self.type in IFS_SERIES:
            f_1 = 1.483 / 1e3
            f_2 = 3.387 / 1e3
            f_3 = 29.49 / 1e3
        elif self.type == 'NCPA':
            f_1 = 0.00137563
            f_2 = 0.00266932
            f_3 = 0.0201064
        elif self.type == 'CTBTO-IMS':
            f_1 = 0.000742936
            f_2 = 0.00169348
            f_3 = 0.014744

        # Convert to Laplace domain
        s_1 = -2*np.pi*f_1
        s_2 = -2*np.pi*f_2
        s_3 = -2*np.pi*f_3
        poles = [s_1, s_2, s_3]

        z_1 = 0.0
        z_2 = 0.0
        z_3 = 0.0
        zeros = [z_1, z_2, z_3]

        if self.wnrs_diameter:
            # Add pole if WNRS is attached
            s_D = self.compute_wnrs_pole()
            poles.append(s_D)

        return (poles, zeros)
