# -*- coding: utf-8 -*-
"""
Main class for designing metadata

.. module:: metadata

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2016-2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
import os
import re
from pandas import read_json
from obspy.core.inventory import Inventory, Network, Channel
from obspy.core.inventory.response import Response, InstrumentSensitivity
from obspy.core.inventory.util import SampleRate

freq_min_default = 1.0e-3

# SEEDManual 2.4 convention codes
path = '/Users/assink/dev/metadata/pysametadata/core'
fid_seedconv_instr = f'{path}/seedchannels_instr.json'
fid_seedconv_orient = f'{path}/seedchannels_orient.json'
sc_ic = read_json(fid_seedconv_instr)
sc_oc = read_json(fid_seedconv_orient)


class Metadata(object):
    def __init__(self, code=None, name=None, description=None,
                 institute=None, **kwargs):
        """
        Initialises Metadata object, sets up channel library and
        inventory attribute
        """
        self.code = code
        self.name = name
        self.inventory = Inventory([], description, sender=institute)
        return

    def attach_inventory(self, inventory):
        """
        Attach Obspy Inventory object to the Metadata object
        """
        self.inventory = inventory
        return

    def calculate_sensitivity(self, channel, unit_out='COUNTS',
                              unit_out_description='Counts'):
        # Compute overall gain
        sens = 1.0
        for stage in channel.response.response_stages:
            sens *= stage.stage_gain

        # Set channel descriptions
        (_, ic, _) = channel.code
        sc_ic_val = sc_ic.loc[sc_ic['instrument_code'] == ic]
        unit_in = sc_ic_val['unit'].values[0]
        unit_in_description = sc_ic_val['unit_description'].values[0]

        # Set channel instrument attributes
        sens_freq = channel.response.response_stages[0].stage_gain_frequency
        channel.response.instrument_sensitivity = InstrumentSensitivity(
            sens, sens_freq, unit_in, unit_out,
            input_units_description=unit_in_description,
            output_units_description=unit_out_description
            )
        return

    def get_seed_id(self, station, channel):
        """
        Helper function to get seed id back
        """
        self.seed_id = (f'{self.network.code}.'
                        f'{station.code}.'
                        f'{channel.location_code}.'
                        f'{channel.code}')
        return

    def set_channel(self, code, sta, location='', sample_rate=None,
                    dip=False, azimuth=False, latitude=None, longitude=None,
                    elevation=None, starttime=None, endtime=None, **kwargs):
        """
        Set common channel properties
        """
        # Vault is used to store emplacement depth
        cha_lat = latitude if latitude else sta.latitude
        cha_lon = longitude if longitude else sta.longitude
        cha_ele = elevation if elevation else sta.elevation
        channel = Channel(
            code, location,
            cha_lat, cha_lon, cha_ele, sta.vault
            )

        channel.response = Response()
        channel.start_date = starttime
        channel.end_date = endtime
        channel.sample_rate = SampleRate(sample_rate)
        channel.azimuth = azimuth
        channel.dip = dip

        self.get_seed_id(sta, channel)
        time_fmt = '%Y.%j.%H.%M.%S'
        resource_id = f'{starttime.strftime(time_fmt)}.{self.seed_id}'
        channel.__setattr__('seed_id', self.seed_id)
        channel.__setattr__('resource_id', resource_id)

        # Obsolete in StationXML 1.1
        # channel.storage_format = 'Steim2'

        (_, ic, oc) = code
        sc_ic_val = sc_ic.loc[sc_ic['instrument_code'] == ic]
        sc_oc_val = sc_oc.loc[sc_oc['orientation_code'] == oc]
        channel.calibration_units = sc_ic_val['unit'].values[0]
        units_description = sc_ic_val['unit_description'].values[0]
        channel.calibration_units_description = units_description
        channel.description = sc_oc_val['description'].values[0]
        return channel

    def set_network(self, code=None, description=None,
                    restricted_status='open',
                    starttime=None, endtime=None, **kwargs):
        """
        Set up common network level attributes
        """
        self.network = Network(
                code,
                description=description, restricted_status=restricted_status,
                start_date=starttime, end_date=endtime
            )
        return

    def set_station(self, sta, site_info, starttime=None, endtime=None,
                    restricted_status='open'):
        """
        Set up common station level attributes
        """
        sta.site = site_info
        sta.creation_date = starttime
        sta.termination_date = endtime
        sta.start_date = starttime
        sta.end_date = endtime
        sta.restricted_status = restricted_status
        sta.alternate_code = self.name
        return

    def write(self, format='STATIONXML', plot_response=False, freq_min=False,
              write_group=True):
        """
        Write out inventory files to disk
        """
        dashline_single = '-' * 80
        dashline_double = '=' * 80

        group_name = f'{self.code}.{self.name}'
        if not os.path.exists(group_name):
            print(f' - creating directory [ {group_name} ]')
            os.makedirs(group_name)

        print()
        if write_group:
            # Write out inventory in one file
            pf = f'{group_name}/{group_name}.xml'
            print(dashline_double)
            print(self.inventory)
            print()
            print(f'Writing out grouped inventory [ {pf} ]')
            print()
            self.inventory.write(pf, format=format)

        # Write out individual channels
        for net in self.inventory:
            for sta in net:
                print(dashline_double)
                print(sta)

                for cha in sta:
                    invs = self.inventory.select(station=sta.code,
                                                 location=cha.location_code,
                                                 channel=cha.code)
                    print(invs)
                    print(dashline_single)
                    print(cha)
                    print(cha.response)
                    print()

                    if plot_response:
                        if freq_min is False:
                            freq_min = freq_min_default

                        if re.match(r'.GZ\b', cha.code):
                            output = 'ACC'
                        else:
                            output = 'VEL'

                        cha.response.plot(freq_min,
                                          output=output, unwrap_phase=True,
                                          label=cha.seed_id, plot_degrees=True)

                        pf = f'{group_name}/{cha.seed_id}.xml'
                        print(f'Writing out [ {pf} ] ...')
                        print()
                        invs.write(pf, format=format)
        return
