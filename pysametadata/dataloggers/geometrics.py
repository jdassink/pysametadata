# -*- coding: utf-8 -*-
"""
Geometrics datalogger classes

.. module:: geometrics

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2016-2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
from pysametadata.dataloggers.datalogger import Datalogger


class GeoEel(Datalogger):
    def __init__(self, channel, **kwargs):
        self.channel = channel
        Datalogger.__init__(self)

        self.model = 'GeoEel'
        self.type = self.model
        self.description = '{} digitizer'.format(self.model)
        self.manufacturer = 'Geometrics, Inc.'

        self.serial = None
        self.sens_freq = kwargs['sens_freq']
        self.gain = kwargs['gain']

        self.set_equipment()
        return
