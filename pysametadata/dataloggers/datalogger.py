# -*- coding: utf-8 -*-
"""
Datalogger classes

.. module:: datalogger

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2016-2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
from obspy.core.inventory.response import CoefficientsTypeResponseStage
from obspy.core.util.obspy_types import FloatWithUncertainties
from obspy.core.inventory.util import Equipment


class Datalogger(object):
    def __init__(self):
        return

    def set_equipment(self):
        self.resource_id = self.channel.resource_id
        self.channel.data_logger = Equipment(
            type=self.type,
            description=self.description,
            manufacturer=self.manufacturer,
            model=self.model,
            serial_number=self.serial,
            installation_date=self.channel.start_date,
            removal_date=self.channel.end_date,
            calibration_dates=[self.channel.start_date],
            resource_id='Datalogger#{}'.format(self.resource_id)
        )

    def set_response(self, **kwargs):
        stage = kwargs['stage']

        # Compute nominal gain if not provided
        if hasattr(self, 'gain') is False:
            adc_counts = 0.5*(2**self.n_bits)
            self.gain = (adc_counts - 1)/self.V_max

        self.resp_stage = CoefficientsTypeResponseStage(
                stage, self.gain, self.sens_freq,
                'V', 'COUNTS',
                'DIGITAL',
                numerator=[], denominator=[],
                input_units_description='Volts',
                output_units_description='Counts',
                description=self.channel.data_logger.description,
                decimation_input_sample_rate=self.channel.sample_rate,
                decimation_factor=1,
                decimation_offset=0,
                decimation_delay=FloatWithUncertainties(0.0),
                decimation_correction=FloatWithUncertainties(0.0)
            )

        self.channel.response.response_stages.insert(stage-1, self.resp_stage)
        return


class Dummy(Datalogger):
    def __init__(self, channel, **kwargs):
        self.channel = channel
        Datalogger.__init__(self)

        self.type = 'Unknown'
        self.description = 'Unknown datalogger'
        self.manufacturer = '-'
        self.model = 'Unknown'
        self.serial = None

        self.sens_freq = kwargs['sens_freq']
        self.gain = kwargs['gain']

        self.set_equipment()
        return
