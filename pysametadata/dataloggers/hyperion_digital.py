# -*- coding: utf-8 -*-
"""
Hyperion datalogger classes

.. module:: hyperion_digital

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2016-2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
from pysametadata.dataloggers.datalogger import Datalogger


class Hyperion(Datalogger):
    def __init__(self, **kwargs):
        Datalogger.__init__(self)

        self.type = self.model
        self.description = '{} digitizer'.format(self.model)
        self.manufacturer = 'Hyperion Technology Group, Ltd.'
        self.serial = kwargs['serial_number']

        self.V_max = kwargs['V_max']
        self.sens_freq = kwargs['sens_freq']
        return


class IFS5200(Hyperion):
    def __init__(self, channel, **kwargs):
        self.model = 'IFS5200'
        self.channel = channel
        self.n_bits = 24
        Hyperion.__init__(self, **kwargs)

        self.set_equipment()
        return
