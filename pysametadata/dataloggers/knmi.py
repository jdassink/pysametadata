# -*- coding: utf-8 -*-
"""
KNMI datalogger classes

.. module:: knmi

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2016-2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
from pysametadata.dataloggers.datalogger import Datalogger


class KNMIpc(Datalogger):
    def __init__(self, channel, **kwargs):
        self.channel = channel
        Datalogger.__init__(self)

        self.model = 'Infrasound array'
        self.type = 'KNMI pc datalogger'
        self.description = ('Linux kernel 2.4, comedi.org drivers, '
                            'Meinberg GPS167+GPS170, '
                            'National Instruments 6034E 16-bit ADC card')
        self.manufacturer = 'KNMI'

        self.serial = None
        self.V_max = kwargs['V_max']
        self.sens_freq = kwargs['sens_freq']
        self.n_bits = 16

        self.set_equipment()
        return
