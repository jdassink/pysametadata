# -*- coding: utf-8 -*-
"""
Guralp datalogger classes

.. module:: guralp

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2016-2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
from pysametadata.dataloggers.datalogger import Datalogger


class CD24(Datalogger):
    def __init__(self, channel, **kwargs):
        self.channel = channel
        Datalogger.__init__(self)

        self.model = 'CD24'
        self.type = self.model
        self.description = '{} digitizer'.format(self.model)
        self.manufacturer = 'Guralp Systems, Ltd.'

        self.n_bits = 24
        self.sens_freq = kwargs['sens_freq']

        # The (8) CD24S3 units have a sensitivity of ~ 0.96E-6 V/count
        # (nominal = 0.9E-6 V / counts )
        # Seems like a 24-bit digitiser with 8.1 Vpp:
        # 8.1 V / (0,5*2**24) Cnt = 0.96e-6 V / Count
        sensitivity = {
            'C545': 0.962e-6,
            'C546': 0.958e-6,
            'C547': 0.959e-6,
            'C549': 0.932e-6,
            'C550': 0.955e-6,
            'C551': 0.956e-6,
            'C552': 0.958e-6,
            'C553': 0.959e-6,
            'C556': 0.959e-6,
            'C557': 0.960e-6
        }

        if kwargs['serial_number']:
            self.serial = kwargs['serial_number']
            self.gain = 1.0 / sensitivity[self.serial]
        else:
            self.serial = None
            self.gain = 1.0

        self.set_equipment()
        return


class CD24E1(Datalogger):
    def __init__(self, channel, **kwargs):
        self.channel = channel
        Datalogger.__init__(self)

        self.model = 'CD24E1'
        self.type = self.model
        self.description = '{} digitizer'.format(self.model)
        self.manufacturer = 'Guralp Systems, Ltd.'

        self.n_bits = 24
        self.sens_freq = kwargs['sens_freq']

        # Sensitivity information from calibration data sheets Guralp
        # The (11) CD24E1 units have a sensitivity of ~ 0.49E-6 V/count.
        # Seems like a 24-bit digitiser with 4.15 Vpp:
        # 4.15 V / (0,5*2**24) Cnt = 0.49e-6 V / Count
        sensitivity = {
            'C206': 0.480e-6,
            'C207': 0.482e-6,
            'C209': 0.480e-6,
            'C325': 0.490e-6,
            'C326': 0.491e-6,
            'C558': 0.483e-6,
            'C563': 0.483e-6,
            'C569': 0.483e-6,
            'C571': 0.488e-6,
            'C573': 0.485e-6,
            'C576': 0.484e-6,
            'DH12': 0.488e-6
        }

        if kwargs['serial_number']:
            self.serial = kwargs['serial_number']
            self.gain = 1.0 / sensitivity[self.serial]
        else:
            self.serial = None
            self.gain = 1.0

        self.set_equipment()
        return
