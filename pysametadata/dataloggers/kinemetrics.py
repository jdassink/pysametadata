# -*- coding: utf-8 -*-
"""
Kinemetrics datalogger classes

.. module:: kinemetrics

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2016-2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
from pysametadata.dataloggers.datalogger import Datalogger


class Kinemetrics(Datalogger):
    def __init__(self, **kwargs):
        Datalogger.__init__(self)

        self.type = self.model
        self.description = '{} digitizer'.format(self.model)
        self.manufacturer = 'Kinemetrics, Inc.'
        self.serial = kwargs['serial_number']

        self.V_max = kwargs['V_max']
        self.sens_freq = kwargs['sens_freq']
        return


class Granite(Kinemetrics):
    def __init__(self, channel, **kwargs):
        self.model = 'Granite'
        self.channel = channel
        self.n_bits = 24
        Kinemetrics.__init__(self, **kwargs)

        self.set_equipment()
        return


class Obsidian(Kinemetrics):
    def __init__(self, channel, **kwargs):
        self.model = 'Obsidian'
        self.channel = channel
        self.n_bits = 24
        Kinemetrics.__init__(self, **kwargs)

        self.set_equipment()
        return


class QuanterraQ330(Kinemetrics):
    def __init__(self, channel, **kwargs):
        self.model = 'Quanterra Q330'
        self.channel = channel
        self.n_bits = 24
        Kinemetrics.__init__(self, **kwargs)

        self.set_equipment()
        return
