#!/usr/bin/env python
# # -*- coding: utf-8 -*-
"""
Metadata for 't Haantje Seismo-Acoustic Array (L206)

.. module:: NL.L206

:author:
    Jelle Assink (jelle.assink@knmi.nl)
    Elmer Ruijgrok, Gert-Jan van den Hazel

:copyright:
    2016-2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
from obspy.core.inventory.util import Site
from obspy.core.inventory import Station
from obspy import UTCDateTime
from obspy.clients.nrl import NRL

from pysametadata.core.metadata import Metadata
# Instrument classes
from pysametadata.sensors.ion import SM6
from pysametadata.sensors.knmi import KNMImb
from pysametadata.dataloggers.geometrics import GeoEel
from pysametadata.dataloggers.kinemetrics import Obsidian

NL_L206 = Metadata(code='NL', name='L206_array', institute='KNMI',
                   description='Royal Netherlands Meteorological Institute')
NL_L206.set_network(code=NL_L206.code,
                    description='Netherlands Seismic and Acoustic Network',
                    starttime=UTCDateTime('1993-01-01T00:00:00'))
net = NL_L206.network

site_info = Site('t Haantje Seismo-Acoustic Array',
                 description='t Haantje Seismo-Acoustic Array',
                 town='t Haantje', country='The Netherlands')
# Geophone station inventory
net.stations.append(Station('L2061', 52.80937, 6.82470, 17.64, vault=60.0))
net.stations.append(Station('L2062', 52.81106, 6.82368, 17.44, vault=60.0))
net.stations.append(Station('L2063', 52.80968, 6.82178, 17.68, vault=60.0))
net.stations.append(Station('L2064', 52.81004, 6.82339, 17.25, vault=30.0))
net.stations.append(Station('L2065', 52.81004, 6.82339, 17.25, vault=60.0))
net.stations.append(Station('L2066', 52.81004, 6.82339, 17.25, vault=90.0))
net.stations.append(Station('L2067', 52.81004, 6.82339, 17.25, vault=120.0))

t0 = UTCDateTime('2018-03-02T10:47:00')     # Original installation of geoLOFAR
t1 = UTCDateTime('2019-02-15T12:00:00')     # GeoEels changed for Obsidian
t2 = UTCDateTime('2020-02-03T12:05:00')     # Gain change 20.0 V -> 2.5 V

channels = {}
channels['HH1'] = {'dip':   0, 'azimuth':  False}
channels['HH2'] = {'dip':   0, 'azimuth':  False}
channels['HHZ'] = {'dip': -90, 'azimuth':  0.0}

# Set up connection to Nominal Response Library (NRL)
nrl = NRL()

print('Creating metadata objects ...')
# Geophone elements
for sta in net.stations:
    print(' - station [ {network}.{station} ]'.format(
        network=net.code, station=sta.code))
    NL_L206.set_station(sta, site_info, starttime=t0)

    # GeoEels period
    for cha in channels.keys():
        # Channel content
        channel = NL_L206.set_channel(cha, sta, sample_rate=250.0,
                                      starttime=t0, endtime=t1,
                                      dip=channels[cha]['dip'],
                                      azimuth=channels[cha]['azimuth'])
        my_sensor = SM6(channel, model='SM-6 B 4.5 Hz 375 Ohm')
        my_sensor.set_response(stage=1)
        my_logger = GeoEel(channel, gain=32*1e5,
                           sens_freq=my_sensor.sens_freq)
        my_logger.set_response(stage=2)
        NL_L206.calculate_sensitivity(channel)
        sta.channels.append(channel)

    # Obsidian 20.0V period
    for cha in channels.keys():
        # Channel content
        channel = NL_L206.set_channel(cha, sta, sample_rate=200.0,
                                      starttime=t1, endtime=t2,
                                      dip=channels[cha]['dip'],
                                      azimuth=channels[cha]['azimuth'])
        my_sensor = SM6(channel, model='SM-6 B 4.5 Hz 375 Ohm')
        my_logger = Obsidian(channel, V_max=20.0,
                             sens_freq=my_sensor.sens_freq, serial_number=1192)
        obsidian_keys = ['Kinemetrics',
                         'Rock Family (Basalt, Granite, Dolomite, Obsidian)',
                         '1', '40', 'Non-causal', '200']
        channel.response = nrl.get_datalogger_response(obsidian_keys)
        channel.response.response_stages.pop(0)
        my_sensor.set_response(stage=1)
        NL_L206.calculate_sensitivity(channel)
        sta.channels.append(channel)

    # Obsidian 2.5V period
    for cha in channels.keys():
        # Channel content
        channel = NL_L206.set_channel(cha, sta, sample_rate=200.0,
                                      starttime=t2,
                                      dip=channels[cha]['dip'],
                                      azimuth=channels[cha]['azimuth'])
        my_sensor = SM6(channel, model='SM-6 B 4.5 Hz 375 Ohm')
        my_logger = Obsidian(channel, V_max=2.5,
                             sens_freq=my_sensor.sens_freq, serial_number=1192)
        obsidian_keys = ['Kinemetrics',
                         'Rock Family (Basalt, Granite, Dolomite, Obsidian)',
                         '1', '5', 'Non-causal', '200']
        channel.response = nrl.get_datalogger_response(obsidian_keys)
        channel.response.response_stages.pop(0)
        my_sensor.set_response(stage=1)
        NL_L206.calculate_sensitivity(channel)
        sta.channels.append(channel)

# Microbarometer element

microbarometer = Station('L206', 52.81192, 6.82244, 16.00, vault=1.0)
print(' - station [ %s.%s ]' % (net.code, microbarometer.code))

net.stations.append(microbarometer)
NL_L206.set_station(microbarometer, site_info, starttime=t1)

channel = NL_L206.set_channel('HDF', microbarometer, sample_rate=100.0,
                              starttime=t1)
my_sensor = KNMImb(channel, model='1000s', wnrs_diameter=10.0)
my_logger = Obsidian(channel, V_max=20.0,
                     sens_freq=my_sensor.sens_freq, serial_number=1192)
obsidian_keys = ['Kinemetrics',
                 'Rock Family (Basalt, Granite, Dolomite, Obsidian)',
                 '1', '40', 'Non-causal', '100']
channel.response = nrl.get_datalogger_response(obsidian_keys)
channel.response.response_stages.pop(0)
my_sensor.set_response(stage=1)
NL_L206.calculate_sensitivity(channel)
microbarometer.channels.append(channel)

# Combine the network entries in one inventory
NL_L206.inventory.networks.append(NL_L206.network)

print('Plotting inventory ...')
NL_L206.inventory.plot(projection='local')

print('Writing out inventory ...')
NL_L206.write(plot_response=False)

del(NL_L206)
