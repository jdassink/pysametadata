#!/usr/bin/env python
# # -*- coding: utf-8 -*-
"""
Metadata for LOFAR HDF Infrasound array

.. module:: NL.LOFAR.HDF.py

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2016-2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
from obspy.core.inventory.util import Site
from obspy.core.inventory import Station
from obspy import UTCDateTime
from obspy.clients.nrl import NRL

from pysametadata.core.metadata import Metadata
# Instrument classes
from pysametadata.sensors.knmi import KNMImb
from pysametadata.dataloggers.guralp import CD24

NL_LOFAR = Metadata(code='NL', name='LOFAR', institute='KNMI',
                    description='Royal Netherlands Meteorological Institute')
NL_LOFAR.set_network(code=NL_LOFAR.code,
                     description='Netherlands Seismic and Acoustic Network',
                     starttime=UTCDateTime('1993-01-01T00:00:00'))
net = NL_LOFAR.network


# Station inventory
net.stations.append(
    Station('L302', 52.901047, 6.848749, 16.0, vault=1.0,
            start_date=UTCDateTime('2021-06-15T08:00:00'),
            end_date=None,
            description=[
                dict(installation_date=UTCDateTime('2021-06-15T08:00:00'),
                     removal_date=None,
                     data_logger='CD24', data_logger_serial_number='C549',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0,
                     sens_freq=0.1
                     )
                ],
            site=Site('Exloo', description='Exloo', town='Exloo',
                      country='The Netherlands')
            ))

net.stations.append(
    Station('L311', 52.811785, 6.394668, 14.0, vault=1.0,
            start_date=UTCDateTime('2020-225T17:15:00'),
            end_date=None,
            description=[
                dict(installation_date=UTCDateTime('2020-225T17:15:00'),
                     removal_date=None,
                     data_logger='CD24', data_logger_serial_number='C556',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0,
                     sens_freq=0.1
                     )
                ],
            site=Site('Dwingeloo', description='Dwingeloo', town='Dwingeloo',
                      country='The Netherlands')
            ))

net.stations.append(
    Station('L406', 53.015064, 6.755422, 26.0, vault=1.0,
            start_date=UTCDateTime('2020-225T17:15:00'),
            end_date=None,
            description=[
                dict(installation_date=UTCDateTime('2020-225T17:15:00'),
                     removal_date=None,
                     data_logger='CD24', data_logger_serial_number='C551',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0,
                     sens_freq=0.1
                     )
                ],
            site=Site('Gieten', description='Gieten', town='Gieten',
                      country='The Netherlands')
            ))

# Set up connection to Nominal Response Library (NRL)
nrl = NRL()
CD24_keys = ['Guralp', 'CMG-CD24 (standalone)', '41-50', '41', '100']

print('Creating metadata objects ...')
for sta in net.stations:
    print(' - station [ {network}.{station} ]'.format(
        network=net.code, station=sta.code))
    sta.creation_date = sta.start_date
    sta.termination_date = sta.end_date

    # Add channels
    for item in sta.description:
        channel = NL_LOFAR.set_channel('HDF', sta, sample_rate=100.0,
                                       starttime=item['installation_date'],
                                       endtime=item['removal_date'])
        if item['sensor'] == 'KNMImb':
            my_sensor = KNMImb(channel, model=item['sensor_model'],
                               sens_freq=item['sens_freq'],
                               wnrs_diameter=item['wnrs_diameter'])
        if item['data_logger'] == 'CD24':
            my_logger = CD24(channel,
                             serial_number=item['data_logger_serial_number'],
                             sens_freq=my_sensor.sens_freq)
            nrl_response = nrl.get_datalogger_response(CD24_keys)
            #nrl_response.response_stages[2].stage_gain = my_logger.gain

        channel.response = nrl_response
        channel.response.response_stages.pop(0)
        my_sensor.set_response(stage=1)
        NL_LOFAR.calculate_sensitivity(channel)
        sta.channels.append(channel)

    # Void the 'description' attribute as it cannot be written to StationXML
    sta.description = None

# Combine the network entries in one inventory
NL_LOFAR.inventory.networks.append(NL_LOFAR.network)

print('Plotting inventory ...')
NL_LOFAR.inventory.plot(projection='local', resolution='f')

print('Writing out inventory ...')
NL_LOFAR.write(plot_response=True)

del(NL_LOFAR)
