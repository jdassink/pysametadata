#!/usr/bin/env python
# # -*- coding: utf-8 -*-
"""
Metadata for Mobile Seismo-Acoustic Array 0 element 1 (MSA01)

.. module:: NL.MSA01

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
from obspy.core.inventory.util import Site
from obspy.core.inventory import Station
from obspy import UTCDateTime
from obspy.clients.nrl import NRL

from pysametadata.core.metadata import Metadata
# Instrument classes
from pysametadata.sensors.ion import SM6
from pysametadata.sensors.knmi import KNMImb
from pysametadata.sensors.hyperion import IFS5000
from pysametadata.dataloggers.kinemetrics import Obsidian

NL_MSA = Metadata(code='NL', name='MSA01_array_temp', institute='KNMI',
                  description='Royal Netherlands Meteorological Institute')
NL_MSA.set_network(code=NL_MSA.code,
                   description='Netherlands Seismic and Acoustic Network',
                   starttime=UTCDateTime('1993-01-01T00:00:00'))
net = NL_MSA.network

site_info = Site('Mobile Seismoacoustic Array 0 - Element 1 (MSA01)',
                 description='Mobile Seismoacoustic Array',
                 town='De Bilt', country='The Netherlands')
# Station inventory
net.stations.append(Station('DBN01', 52.09887, 5.17589, 4.0, vault=1.0))
net.stations.append(Station('DBN02', 52.09897, 5.17621, 3.0, vault=1.0))
net.stations.append(Station('DBN03', 52.09897, 5.17685, 1.0, vault=1.0))
net.stations.append(Station('DBN04', 52.09875, 5.17683, 2.0, vault=1.0))
net.stations.append(Station('DBN05', 52.09842, 5.17653, 2.0, vault=1.0))
net.stations.append(Station('DBN06', 52.09853, 5.17623, 3.0, vault=1.0))
net.stations.append(Station('MSA01', 52.09989, 5.17647, 0.0, vault=0.0))

t0 = UTCDateTime('2020-06-26T14:00:00')

channels = {}
channels['HH1'] = {'dip':   0, 'azimuth': None }
channels['HH2'] = {'dip':   0, 'azimuth': None }
channels['HHZ'] = {'dip': -90, 'azimuth': None }
channels['HDF'] = {'dip': None, 'azimuth': None}

# Set up connection to Nominal Response Library (NRL)
nrl = NRL()

print('Creating metadata objects ...')
for sta in net.stations:
    print(' - station [ {network}.{station} ]'.format(
        network=net.code, station=sta.code))
    NL_MSA.set_station(sta, site_info, starttime=t0)

    # Channel content for the mobile array element
    if sta.code == 'MSA01':
        for cha in channels.keys():
            if cha == 'HDF':
                # Infrasound channel
                channel = NL_MSA.set_channel(cha, sta, location='01',
                                             sample_rate=200.0,
                                             starttime=t0)
                my_sensor = IFS5000(channel)
                my_logger = Obsidian(channel, V_max=20.0,
                                     sens_freq=my_sensor.sens_freq,
                                     serial_number=1254)
                obsidian_keys = [
                    'Kinemetrics',
                    'Rock Family (Basalt, Granite, Dolomite, Obsidian)',
                    '1', '40', 'Non-causal', '200']
                channel.response = nrl.get_datalogger_response(obsidian_keys)
                channel.response.response_stages.pop(0)
                my_sensor.set_response(stage=1)
                #my_logger.set_response(stage=2)
            else:
                # Seismic channels
                channel = NL_MSA.set_channel(cha, sta, location='01',
                                             sample_rate=200.0,
                                             starttime=t0,
                                             dip=channels[cha]['dip'],
                                             azimuth=channels[cha]['azimuth'])
                my_sensor = SM6(channel, model='SM-6 B 4.5 Hz 375 Ohm')
                my_logger = Obsidian(channel, V_max=2.5,
                                     sens_freq=my_sensor.sens_freq,
                                     serial_number=1254)
                obsidian_keys = [
                    'Kinemetrics',
                    'Rock Family (Basalt, Granite, Dolomite, Obsidian)',
                    '1', '5', 'Non-causal', '200']
                channel.response = nrl.get_datalogger_response(obsidian_keys)
                channel.response.response_stages.pop(0)
                my_sensor.set_response(stage=1)
                #my_logger.set_response(stage=2)

            NL_MSA.calculate_sensitivity(channel)
            sta.channels.append(channel)

    # Channel content for DBNI components -- for comparison purposes.
    # This part should be removed when NL.MSA01 is operational.
    else:
        channel = NL_MSA.set_channel('HDF', sta, location='01',
                                     sample_rate=100.0, starttime=t0)
        my_sensor = KNMImb(channel, model='500s', wnrs_diameter=8.0)
        my_logger = Obsidian(channel, V_max=20.0,
                             sens_freq=my_sensor.sens_freq, serial_number=454)
        obsidian_keys = [
            'Kinemetrics',
            'Rock Family (Basalt, Granite, Dolomite, Obsidian)',
            '1', '40', 'Non-causal', '100']
        channel.response = nrl.get_datalogger_response(obsidian_keys)
        channel.response.response_stages.pop(0)
        my_sensor.set_response(stage=1)
        #my_logger.set_response(stage=2)

        NL_MSA.calculate_sensitivity(channel)
        sta.channels.append(channel)

# Combine the network entries in one inventory
NL_MSA.inventory.networks.append(NL_MSA.network)

print('Plotting inventory ...')
NL_MSA.inventory.plot(projection='local', resolution='f')

print('Writing out inventory ...')
NL_MSA.write(plot_response=True)

del(NL_MSA)
