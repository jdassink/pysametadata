#!/usr/bin/env python
# # -*- coding: utf-8 -*-
"""
Metadata for Spijk Seismo-Acoustic Array (L509)

.. module:: NL.L509

:author:
    Jelle Assink (jelle.assink@knmi.nl)
    Elmer Ruijgrok, Gert-Jan van den Hazel

:copyright:
    2016-2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
from obspy.core.inventory.util import Site
from obspy.core.inventory import Station
from obspy import UTCDateTime
from obspy.clients.nrl import NRL

from pysametadata.core.metadata import Metadata
# Instrument classes
from pysametadata.sensors.ion import SM6
from pysametadata.sensors.knmi import KNMImb
from pysametadata.dataloggers.geometrics import GeoEel
from pysametadata.dataloggers.kinemetrics import Obsidian

NL_L509 = Metadata(code='NL', name='L509_array', institute='KNMI',
                   description='Royal Netherlands Meteorological Institute')
NL_L509.set_network(code=NL_L509.code,
                    description='Netherlands Seismic and Acoustic Network',
                    starttime=UTCDateTime('1993-01-01T00:00:00'))
net = NL_L509.network

site_info = Site('Spijk Seismo-Acoustic Array',
                 description='Spijk Seismo-Acoustic Array',
                 town='Spijk', country='The Netherlands')
# Geophone station inventory
t0 = UTCDateTime('2020-310T00:00:00')     # Original installation of L509

# Set up connection to Nominal Response Library (NRL)
nrl = NRL()

print('Creating metadata objects ...')
# Microbarometer element

microbarometer = Station('L509', 53.408419, 6.784943, -2.0, vault=1.0)
print(' - station [ %s.%s ]' % (net.code, microbarometer.code))

net.stations.append(microbarometer)
NL_L509.set_station(microbarometer, site_info, starttime=t0)

channel = NL_L509.set_channel('HDF', microbarometer, sample_rate=100.0,
                              starttime=t0)
my_sensor = KNMImb(channel, model='1000s', wnrs_diameter=10.0)
my_logger = Obsidian(channel, V_max=20.0,
                     sens_freq=my_sensor.sens_freq, serial_number=561)
obsidian_keys = ['Kinemetrics',
                 'Rock Family (Basalt, Granite, Dolomite, Obsidian)',
                 '1', '40', 'Non-causal', '100']
channel.response = nrl.get_datalogger_response(obsidian_keys)
channel.response.response_stages.pop(0)
my_sensor.set_response(stage=1)
NL_L509.calculate_sensitivity(channel)
microbarometer.channels.append(channel)

# Combine the network entries in one inventory
NL_L509.inventory.networks.append(NL_L509.network)

print('Plotting inventory ...')
NL_L509.inventory.plot(projection='local')

print('Writing out inventory ...')
NL_L509.write(plot_response=False)

del(NL_L509)
