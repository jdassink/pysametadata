from obspy import read_inventory
from waveforms.metadata import inv_to_df, df_to_ascii
import sys
import os
from argparse import ArgumentParser

def main(argv):
    args = process_cli(argv)
    fid_in = args.xml_file

    inv = read_inventory(fid_in)
    df = inv_to_df(inv, compute_offset=True, offset_reference='first')

    print(df)
    if args.ascii_file:
        fid_out = args.ascii_file
        df_to_ascii(df, fid_out)
    return

def process_cli(argv):
    parser = ArgumentParser()

    parser.add_argument('-f', '--xml_file',
        type=str,
        metavar='StationXML file',
        required=True
        )

    parser.add_argument('-o', '--ascii_file',
        type=str,
        metavar='Output ASCII file',
        required=False
        )

    parser.add_argument('-v', dest='verbose', action='store_true')
    args = parser.parse_args()
    return args

if __name__ == "__main__":
   main(sys.argv[1:])
