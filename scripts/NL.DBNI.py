#!/usr/bin/env python
# # -*- coding: utf-8 -*-
"""
Metadata for De Bilt Infrasound Array (DBNI)

.. module:: NL.DBNI

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
from obspy.core.inventory.util import Site
from obspy.core.inventory import Station
from obspy import UTCDateTime
from obspy.clients.nrl import NRL

from pysametadata.core.metadata import Metadata
# Instrument classes
from pysametadata.sensors.knmi import KNMImb
from pysametadata.sensors.hyperion import IFS5000
from pysametadata.dataloggers.kinemetrics import Granite, Obsidian

NL_DBNI = Metadata(code='NL', name='DBNI', institute='KNMI',
                  description='Royal Netherlands Meteorological Institute')
NL_DBNI.set_network(code=NL_DBNI.code,
                   description='Netherlands Seismic and Acoustic Network',
                   starttime=UTCDateTime('1993-01-01T00:00:00'))
net = NL_DBNI.network

site_info = Site('De Bilt Infrasound Array',
                 description='De Bilt Infrasound Array',
                 town='De Bilt', country='The Netherlands')
# Station inventory
net.stations.append(Station('DBN01', 52.098852, 5.175884, 3.0, vault=1.0))
net.stations.append(Station('DBN02', 52.098966, 5.176202, 3.0, vault=1.0))
net.stations.append(Station('DBN03', 52.098963, 5.176834, 3.0, vault=1.0))
net.stations.append(Station('DBN04', 52.098737, 5.176832, 3.0, vault=1.0))
net.stations.append(Station('DBN05', 52.098402, 5.176514, 3.0, vault=1.0))
net.stations.append(Station('DBN06', 52.098516, 5.176198, 3.0, vault=1.0))
net.stations.append(Station('DBN07', 52.099922, 5.177606, 3.0, vault=1.0))
net.stations.append(Station('DBN08', 52.099134, 5.179720, 2.0, vault=1.0))
# Temporary location of reference Hyperion sensor close to C-cabinet
#net.stations.append(Station('DBN08', 52.098646, 5.176704, 2.0, vault=1.0))

# Original installation DBNI with KNMI-mb (NL.DBN0?..BDF)
t0 = UTCDateTime('2002-233T14:30:00')
# Installation Granite datalogger installation DBNI (NL.DBN0?.00.HDF)
t1 = UTCDateTime('2014-10-05T00:00:00')
# Installation Obsidian datalogger installation DBNI (NL.DBN0?.01.HDF)
t2 = UTCDateTime('2016-176T10:00:00')

# First installation Hyperion at NL.DBN04.02.HDF (with porous hoses)
t3 = UTCDateTime('2021-04-16T18:00:00')
# Connection of Baroport to Hyperion (NL.DBN04.02.HDF)
t4 = UTCDateTime('2021-04-28T13:30:00')
# Installation of reference Hyperion (NL.DBN08.02.HDF) under dome
t5 = UTCDateTime('2021-06-14T10:57:00')
# Change sample rate from 100 to 500 Hz for NL.DBN04.02.HDF and NL.DBN08.02.HDF
t6 = UTCDateTime('2021-08-18T05:30:00')
# Add bare Hyperion to NL.DBN04 site with both pressure and acceleration signal at 500 Hz
# note: from ~16:00 UTC on 11 Nov. 2021, the sensor is within the data cabinet.
t7 = UTCDateTime('2021-11-11T14:26:00')

# Set up connection to Nominal Response Library (NRL)
nrl = NRL()
# Obtain response information for Obsidian digitiser to be used later
# obsidian_keys = ['Kinemetrics',
#                  'Rock Family (Basalt, Granite, Dolomite, Obsidian)',
#                  '1', '40', 'Non-causal', '100']

print('Creating metadata objects ...')
for sta in net.stations:
    print(' - station [ {network}.{station} ]'.format(
        network=net.code, station=sta.code))
    NL_DBNI.set_station(sta, site_info, starttime=t0)

    if sta.code not in ['DBN07', 'DBN08']:
        # ---------------------------------------------------------------------
        # Channel content for the KNMI-mb array with Granite in K cabinet
        srate = 100.0
        channel = NL_DBNI.set_channel('HDF', sta, location='00',
                                      sample_rate=srate, starttime=t1, endtime=t2)
        my_sensor = KNMImb(channel, model='500s', sens_freq=0.1, wnrs_diameter=10.0)
        #my_sensor = KNMImb(channel, model='500s', sens_freq=1.0, wnrs_diameter=False)
        my_logger = Obsidian(channel, V_max=20.0,
                             sens_freq=my_sensor.sens_freq, serial_number=436)
        # channel.response = nrl.get_datalogger_response(logger_keys)
        # channel.response.response_stages.pop(0)
        my_sensor.set_response(stage=1)
        my_logger.set_response(stage=2)   # Comment this line when using NRL response
        NL_DBNI.calculate_sensitivity(channel)
        sta.channels.append(channel)

        # ---------------------------------------------------------------------
        # Channel content for the KNMI-mb array with Obsidian in K cabinet
        channel = NL_DBNI.set_channel('HDF', sta, location='01',
                                      sample_rate=srate, starttime=t2)
        my_sensor = KNMImb(channel, model='500s', sens_freq=0.1, wnrs_diameter=10.0)
        my_logger = Obsidian(channel, V_max=20.0,
                             sens_freq=my_sensor.sens_freq, serial_number=454)
        my_sensor.set_response(stage=1)
        my_logger.set_response(stage=2)   # Comment this line when using NRL response
        NL_DBNI.calculate_sensitivity(channel)
        sta.channels.append(channel)
    
    # ---------------------------------------------------------------------
    # Channel content for the Hyperion array with datalogger in C cabinet
    if sta.code == 'DBN04':
        # NL.DBN04.02.HDF (Hyperion sensor) connected to porous hoses
        channel = NL_DBNI.set_channel('HDF', sta, location='02',
                                      sample_rate=100.0,
                                      starttime=t3, endtime=t4)
        my_sensor = IFS5000(channel, wnrs_diameter=10.0, sens_freq=0.25)
        my_logger = Obsidian(channel, V_max=20.0,
                            sens_freq=my_sensor.sens_freq,
                            serial_number=1254)
        my_sensor.set_response(stage=1)
        my_logger.set_response(stage=2)
        NL_DBNI.calculate_sensitivity(channel)
        sta.channels.append(channel)

        # NL.DBN04.02.HDF (Hyperion sensor + baroport + dome) connected to the 
        # Baroport which has characteristics of a high-pass filter around 1 Hz
        channel = NL_DBNI.set_channel('HDF', sta, location='02',
                                      sample_rate=100.0,
                                      starttime=t4, endtime=t6)
        my_sensor = IFS5000(channel, wnrs_diameter=18.0, sens_freq=0.25)
        my_logger = Obsidian(channel, V_max=20.0,
                            sens_freq=my_sensor.sens_freq,
                            serial_number=1254)
        my_sensor.set_response(stage=1)
        my_logger.set_response(stage=2)
        NL_DBNI.calculate_sensitivity(channel)
        sta.channels.append(channel)

        # NL.DBN04.02.HDF (Hyperion sensor + baroport + dome) set to 500 Hz
        channel = NL_DBNI.set_channel('HDF', sta, location='02',
                                      sample_rate=500.0,
                                      starttime=t6)
        my_sensor = IFS5000(channel, wnrs_diameter=18.0, sens_freq=0.25)
        my_logger = Obsidian(channel, V_max=20.0,
                            sens_freq=my_sensor.sens_freq,
                            serial_number=1254)
        my_sensor.set_response(stage=1)
        my_logger.set_response(stage=2)
        NL_DBNI.calculate_sensitivity(channel)
        sta.channels.append(channel)
    
        # NL.DBN04.RF.HDF (Hyperion sensor no dome) set to 500 Hz
        channel = NL_DBNI.set_channel('HDF', sta, location='RF',
                                      sample_rate=500.0,
                                      starttime=t7)
        my_sensor = IFS5000(channel, wnrs_diameter=None, sens_freq=1.0)
        my_logger = Obsidian(channel, V_max=20.0,
                            sens_freq=my_sensor.sens_freq,
                            serial_number=1254)
        my_sensor.set_response(stage=1)
        my_logger.set_response(stage=2)
        NL_DBNI.calculate_sensitivity(channel)
        sta.channels.append(channel)

        channel = NL_DBNI.set_channel('HGZ', sta, location='RF',
                                      sample_rate=500.0,
                                      starttime=t7)
        my_sensor = IFS5000(channel, wnrs_diameter=None, sens_freq=1.0)
        my_logger = Obsidian(channel, V_max=20.0,
                             sens_freq=my_sensor.sens_freq,
                             serial_number=1254)
        my_sensor.set_response(stage=1)
        my_logger.set_response(stage=2)
        NL_DBNI.calculate_sensitivity(channel)
        sta.channels.append(channel)

    elif sta.code == 'DBN08': # or sta.code == 'DBN07':
        # NL.DBN08.02.HDF (Hyperion sensor + dome), installed as reference sensor
        channel = NL_DBNI.set_channel('HDF', sta, location='02',
                                      sample_rate=100.0,
                                      starttime=t5, endtime=t6)
        my_sensor = IFS5000(channel, wnrs_diameter=None, sens_freq=1.0)
        my_logger = Obsidian(channel, V_max=20.0,
                            sens_freq=my_sensor.sens_freq,
                            serial_number=1254)
        my_sensor.set_response(stage=1)
        my_logger.set_response(stage=2)
        NL_DBNI.calculate_sensitivity(channel)
        sta.channels.append(channel)

        # NL.DBN08.02.HDF (Hyperion sensor + dome) set to 500 Hz
        channel = NL_DBNI.set_channel('HDF', sta, location='02',
                                      sample_rate=500.0,
                                      starttime=t6)
        my_sensor = IFS5000(channel, wnrs_diameter=None, sens_freq=1.0)
        my_logger = Obsidian(channel, V_max=20.0,
                            sens_freq=my_sensor.sens_freq,
                            serial_number=1254)
        my_sensor.set_response(stage=1)
        my_logger.set_response(stage=2)
        NL_DBNI.calculate_sensitivity(channel)
        sta.channels.append(channel)

# Combine the network entries in one inventory
NL_DBNI.inventory.networks.append(NL_DBNI.network)

print('Plotting inventory ...')
NL_DBNI.inventory.plot(projection='local', resolution='f')

print('Writing out inventory ...')
NL_DBNI.write(plot_response=True)

del(NL_DBNI)
