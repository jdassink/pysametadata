#!/usr/bin/env python
# # -*- coding: utf-8 -*-
"""
Metadata for LOFAR BDF Infrasound array

.. module:: NL.LOFAR.BDF.py

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2016-2021, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
from obspy.core.inventory.util import Site
from obspy.core.inventory import Station
from obspy import UTCDateTime
from obspy.clients.nrl import NRL

from pysametadata.core.metadata import Metadata
# Instrument classes
from pysametadata.sensors.knmi import KNMImb
from pysametadata.dataloggers.guralp import CD24, CD24E1

NL_LOFAR = Metadata(code='NL', name='LOFAR', institute='KNMI',
                    description='Royal Netherlands Meteorological Institute')
NL_LOFAR.set_network(code=NL_LOFAR.code,
                     description='Netherlands Seismic and Acoustic Network',
                     starttime=UTCDateTime('1993-01-01T00:00:00'))
net = NL_LOFAR.network

# Station inventory
net.stations.append(
    Station('IS103', 52.915210, 6.896464, 6.0, vault=1.0,
            start_date=UTCDateTime('2011-292T00:00:00'),
            end_date=UTCDateTime('2020-157T00:00:00'),
            description=[
                dict(installation_date=UTCDateTime('2011-292T00:00:00'),
                     removal_date=UTCDateTime('2020-157T00:00:00'),
                     data_logger='CD24', data_logger_serial_number='C556',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     )
                ],
            site=Site('Exloerveen', description='Exloerveen',
                      town='Exloerveen', country='The Netherlands')
            ))

net.stations.append(
    Station('IS106', 52.873598, 6.984754, 8.0, vault=1.0,
            start_date=UTCDateTime('2011-179T00:00:00'),
            end_date=UTCDateTime('2019-199T00:00:00'),
            description=[
                dict(installation_date=UTCDateTime('2011-179T00:00:00'),
                     removal_date=UTCDateTime('2019-199T00:00:00'),
                     data_logger='CD24', data_logger_serial_number='C547',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     )
                ],
            site=Site('Valthermond Seismo-Acoustic Array',
                      description='Valthermond Seismo-Acoustic Array',
                      town='Valthermond', country='The Netherlands')
            ))

net.stations.append(
    Station('IS107', 52.921273, 7.096486, 12.0, vault=1.0,
            start_date=UTCDateTime('2012-319T00:00:00'),
            end_date=UTCDateTime('2020-157T00:00:00'),
            description=[
                dict(installation_date=UTCDateTime('2012-319T00:00:00'),
                     removal_date=UTCDateTime('2020-157T00:00:00'),
                     data_logger='CD24E1', data_logger_serial_number='DH12',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     )
                ],
            site=Site('Ter Wisch Seismo-Acoustic Array',
                      description='Ter Wisch Seismo-Acoustic Array',
                      town='Ter Wisch', country='The Netherlands')
            ))

net.stations.append(
    Station('IS205', 52.857911, 6.896994, 9.0, vault=1.0,
            start_date=UTCDateTime('2011-160T00:00:00'),
            end_date=UTCDateTime('2019-229T00:00:00'),
            description=[
                dict(installation_date=UTCDateTime('2011-160T00:00:00'),
                     removal_date=UTCDateTime('2017-09-07T12:00:00'),
                     data_logger='CD24E1', data_logger_serial_number='C326',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     ),
                dict(installation_date=UTCDateTime('2018-07-21T00:00:00'),
                     removal_date=UTCDateTime('2019-229T00:00:00'),
                     data_logger='CD24E1', data_logger_serial_number='C571',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     )
                ],
            site=Site('Valthe', description='Valthe',
                      town='Valthe', country='The Netherlands')
            ))

net.stations.append(
    Station('IS206', 52.811922, 6.822437, 16.0, vault=1.0,
            start_date=UTCDateTime('2011-304T00:00:00'),
            end_date=UTCDateTime('2019-02-15T12:00:00'),
            description=[
                dict(installation_date=UTCDateTime('2011-304T00:00:00'),
                     removal_date=UTCDateTime('2019-02-15T12:00:00'),
                     data_logger='CD24', data_logger_serial_number='C551',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     )
                ],
            site=Site('t Haantje Seismo-Acoustic Array',
                      description='t Haantje Seismo-Acoustic Array',
                      town='t Haantje', country='The Netherlands')
            ))

net.stations.append(
    Station('IS207', 52.757875, 6.972268, 21.0, vault=1.0,
            start_date=UTCDateTime('2011-227T00:00:00'),
            end_date=UTCDateTime('2018-047T12:00:00'),
            description=[
                dict(installation_date=UTCDateTime('2011-227T00:00:00'),
                     removal_date=UTCDateTime('2018-047T12:00:00'),
                     data_logger='CD24', data_logger_serial_number='C550',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     )
                ],
            site=Site('Nieuw-Dordrecht Seismo-Acoustic Array',
                      description='Nieuw-Dordrecht Seismo-Acoustic Array',
                      town='Nieuw-Dordrecht', country='The Netherlands')
            ))

net.stations.append(
    Station('IS208', 52.668713, 6.917914, 12.0, vault=1.0,
            start_date=UTCDateTime('2012-319T00:00:00'),
            end_date=UTCDateTime('2019-02-15T14:00:00'),
            description=[
                dict(installation_date=UTCDateTime('2012-319T00:00:00'),
                     removal_date=UTCDateTime('2019-02-15T14:00:00'),
                     data_logger='CD24', data_logger_serial_number='C546',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     )
                ],
            site=Site('Schoonebeek Seismo-Acoustic Array',
                      description='Schoonebeek Seismo-Acoustic Array',
                      town='Schoonebeek', country='The Netherlands')
            ))

net.stations.append(
    Station('IS302', 52.901047, 6.848749, 16.0, vault=1.0,
            start_date=UTCDateTime('2011-160T00:00:00'),
            end_date=UTCDateTime('2020-230T06:05:00'),
            description=[
                dict(installation_date=UTCDateTime('2011-160T00:00:00'),
                     removal_date=UTCDateTime('2020-230T06:05:00'),
                     data_logger='CD24E1', data_logger_serial_number='C569',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     )
                ],
            site=Site('Exloo', description='Exloo',
                      town='Exloo', country='The Netherlands')
            ))

net.stations.append(
    Station('IS307', 52.80384, 6.67971, 20.0, vault=1.0,
            start_date=UTCDateTime('2011-160T00:00:00'),
            end_date=UTCDateTime('2019-229T00:00:00'),
            description=[
                dict(installation_date=UTCDateTime('2011-160T00:00:00'),
                     removal_date=UTCDateTime('2017-09-14T12:00:00'),
                     data_logger='CD24E1', data_logger_serial_number='C576',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     ),
                dict(installation_date=UTCDateTime('2017-09-14T12:00:00'),
                     removal_date=UTCDateTime('2019-229T00:00:00'),
                     data_logger='CD24E1', data_logger_serial_number='C558',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     )
                ],
            site=Site('Aalden', description='Aalden',
                      town='Aalden', country='The Netherlands')
            ))

net.stations.append(
    Station('IS311', 52.811785, 6.394668, 14.0, vault=1.0,
            start_date=UTCDateTime('2012-319T00:00:00'),
            end_date=UTCDateTime('2020-225T18:00:00'),
            description=[
                dict(installation_date=UTCDateTime('2012-319T00:00:00'),
                     removal_date=UTCDateTime('2020-06-04T15:00:00'),
                     data_logger='CD24', data_logger_serial_number='C552',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     ),
                dict(installation_date=UTCDateTime('2020-06-04T16:30:00'),
                     removal_date=UTCDateTime('2020-225T18:00:00'),
                     data_logger='CD24', data_logger_serial_number='C556',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     )
                ],
            site=Site('Dwingeloo', description='Dwingeloo',
                      town='Dwingeloo', country='The Netherlands')
            ))

net.stations.append(
    Station('IS406', 53.015064, 6.755422, 26.0, vault=1.0,
            start_date=UTCDateTime('2011-229T00:00:00'),
            end_date=UTCDateTime('2020-225T18:00:00'),
            description=[
                dict(installation_date=UTCDateTime('2011-229T00:00:00'),
                     removal_date=UTCDateTime('2020-154T12:00:00'),
                     data_logger='CD24', data_logger_serial_number='C553',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     ),
                dict(installation_date=UTCDateTime('2020-154T16:00:00'),
                     removal_date=UTCDateTime('2020-225T18:00:00'),
                     data_logger='CD24', data_logger_serial_number='C551',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     )
                ],
            site=Site('Gieten', description='Gieten',
                      town='Gieten', country='The Netherlands')
            ))

net.stations.append(
    Station('IS411', 53.035510, 6.635690, 3.0, vault=1.0,
            start_date=UTCDateTime('2012-319T00:00:00'),
            end_date=UTCDateTime('2019-229T00:00:00'),
            description=[
                dict(installation_date=UTCDateTime('2012-319T00:00:00'),
                     removal_date=UTCDateTime('2017-11-28T15:00:00'),
                     data_logger='CD24E1', data_logger_serial_number='C209',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     ),
                dict(installation_date=UTCDateTime('2017-11-28T15:00:00'),
                     removal_date=UTCDateTime('2019-229T00:00:00'),
                     data_logger='CD24E1', data_logger_serial_number='C563',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     )
                ],
            site=Site('Taarlo Seismo-Acoustic Array',
                      description='Taarlo Seismo-Acoustic Array',
                      town='Taarlo', country='The Netherlands')
            ))

net.stations.append(
    Station('IS412', 52.914790, 6.606110, 19.0, vault=1.0,
            start_date=UTCDateTime('2011-221T00:00:00'),
            end_date=UTCDateTime('2019-229T00:00:00'),
            description=[
                dict(installation_date=UTCDateTime('2011-221T00:00:00'),
                     removal_date=UTCDateTime('2019-229T00:00:00'),
                     data_logger='CD24', data_logger_serial_number='C557',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     )
                ],
            site=Site('Westerbork', description='Westerbork',
                      town='Hooghalen', country='The Netherlands')
            ))

net.stations.append(
    Station('IS503', 52.945337, 6.850156, 3.0, vault=1.0,
            start_date=UTCDateTime('2012-319T00:00:00'),
            end_date=None,
            description=[
                dict(installation_date=UTCDateTime('2012-319T00:00:00'),
                     removal_date=None,
                     data_logger='CD24E1', data_logger_serial_number='C207',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     )
                ],
            site=Site('Buinen', description='Buinen',
                      town='Buinen', country='The Netherlands')
            ))

net.stations.append(
    Station('IS507', 53.06870, 7.00720, 0.0, vault=1.0,
            start_date=UTCDateTime('2011-229T00:00:00'),
            end_date=UTCDateTime('2015-282T00:00:00'),
            description=[
                dict(installation_date=UTCDateTime('2011-229T00:00:00'),
                     removal_date=UTCDateTime('2015-282T00:00:00'),
                     data_logger='CD24E1', data_logger_serial_number='C571',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     )
                ],
            site=Site('Alteveer', description='Alteveer',
                      town='Alteveer', country='The Netherlands')
            ))

net.stations.append(
    Station('IS508', 53.239546, 6.953285, -5.0, vault=1.0,
            start_date=UTCDateTime('2011-227T00:00:00'),
            end_date=UTCDateTime('2019-229T00:00:00'),
            description=[
                dict(installation_date=UTCDateTime('2011-227T00:00:00'),
                     removal_date=UTCDateTime('2019-229T00:00:00'),
                     data_logger='CD24E1', data_logger_serial_number='C206',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     )
                ],
            site=Site('Nieuwolda Seismo-Acoustic Array',
                      description='Nieuwolda Seismo-Acoustic Array',
                      town='Nieuwolda', country='The Netherlands')
            ))

net.stations.append(
    Station('IS509', 53.408419, 6.784943, -2.0, vault=1.0,
            start_date=UTCDateTime('2011-173T00:00:00'),
            end_date=UTCDateTime('2017-253T00:00:00'),
            description=[
                dict(installation_date=UTCDateTime('2011-173T00:00:00'),
                     removal_date=UTCDateTime('2017-253T00:00:00'),
                     data_logger='CD24E1', data_logger_serial_number='C573',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     )
                ],
            site=Site('Spijk Seismo-Acoustic Array',
                      description='Spijk Seismo-Acoustic Array',
                      town='Roodeschool', country='The Netherlands')
            ))

# Set up connection to Nominal Response Library (NRL)
nrl = NRL()
CD24_keys = ['Guralp', 'CMG-CD24 (standalone)', '61-70', '63', '40']
CD24E1_keys = ['Guralp', 'CMG-CD24-E1', '1', '40', '20', '10', '5', '40']

print('Creating metadata objects ...')
for sta in net.stations:
    print(' - station [ {network}.{station} ]'.format(
        network=net.code, station=sta.code))
    sta.creation_date = sta.start_date
    sta.termination_date = sta.end_date

    # Add channels
    for item in sta.description:
        channel = NL_LOFAR.set_channel('BDF', sta, sample_rate=40.0,
                                       starttime=item['installation_date'],
                                       endtime=item['removal_date'])
        if item['sensor'] == 'KNMImb':
            my_sensor = KNMImb(channel, model=item['sensor_model'],
                               wnrs_diameter=item['wnrs_diameter'])
        if item['data_logger'] == 'CD24':
            my_logger = CD24(channel,
                             serial_number=item['data_logger_serial_number'],
                             sens_freq=my_sensor.sens_freq)
            nrl_response = nrl.get_datalogger_response(CD24_keys)
            nrl_response.response_stages[2].stage_gain = my_logger.gain
        elif item['data_logger'] == 'CD24E1':
            my_logger = CD24E1(channel,
                               serial_number=item['data_logger_serial_number'],
                               sens_freq=my_sensor.sens_freq)
            nrl_response = nrl.get_datalogger_response(CD24E1_keys)
            nrl_response.response_stages[2].stage_gain = my_logger.gain

        channel.response = nrl_response
        channel.response.response_stages.pop(0)
        my_sensor.set_response(stage=1)
        NL_LOFAR.calculate_sensitivity(channel)
        sta.channels.append(channel)

    # Void the 'description' attribute as it cannot be written to StationXML
    sta.description = None

# Combine the network entries in one inventory
NL_LOFAR.inventory.networks.append(NL_LOFAR.network)

print('Plotting inventory ...')
NL_LOFAR.inventory.plot(projection='local', resolution='f')

print('Writing out inventory ...')
NL_LOFAR.write(plot_response=True)

del(NL_LOFAR)


"""
There is no data for station IS306

net.stations.append(
    Station('IS306', 52.889637,6.743629, 1.0, vault=1.0,
            start_date=UTCDateTime('2011-160T00:00:00'),
            end_date=UTCDateTime('2017-09-14T00:00:00'),
            description=[
                dict(installation_date=UTCDateTime('2011-160T00:00:00'),
                     removal_date=UTCDateTime('2017-09-14T00:00:00'),
                     data_logger='CD24E1', data_logger_serial_number='C545',
                     sensor='KNMImb', sensor_model='1000s', wnrs_diameter=10.0
                     )
                ],
            site=Site('Ellertshaar', description='Ellertshaar',
                      town='Ellertshaar', country='The Netherlands')
            ))

Never installed (12)
# net.stations.append(Station('IS104',52.943270,6.929930, 0.0,
#                       site=Site(name="Eerste Exloermond, The Netherlands")))
# net.stations.append(Station('IS210',52.301490,6.749320, 0.0,
#                       site=Site(name="Borne, The Netherlands")))
# net.stations.append(Station('IS308',52.822465,6.534501, 0.0,
#                       site=Site(name="Wijster, The Netherlands")))
# net.stations.append(Station('IS309',52.534115,6.527681, 0.0,
#                       site=Site(name="Stegeren, The Netherlands")))
# net.stations.append(Station('IS310',52.770880,6.109490, 0.0,
#                       site=Site(name="Steenwijk, The Netherlands")))
# net.stations.append(Station('IS404',52.928610,6.807460, 0.0,
#                       site=Site(name="Borger, The Netherlands")))
# net.stations.append(Station('IS408',53.028923,6.453337, 0.0,
#                       site=Site(name="Zuidvelde, The Netherlands")))
# net.stations.append(Station('IS409',53.014783,6.339111, 0.0,
#                       site=Site(name="Weperpolder, The Netherlands")))
# net.stations.append(Station('IS410',52.994210,5.830210, 0.0,
#                       site=Site(name="Skipsleat, The Netherlands")))
# net.stations.append(Station('IS413',53.019542,6.199998, 0.0,
#                       site=Site(name="Hornsterzwaag, The Netherlands")))
# net.stations.append(Station('IS506',53.014203,7.025266, 0.0,
#                       site=Site(name="Onstwedde, The Netherlands")))
"""
