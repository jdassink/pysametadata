#!/usr/bin/env python
# # -*- coding: utf-8 -*-
"""
Metadata for Dimona Infrasound (I0BR) array

.. module:: IM.I0BR

:author:
    Jelle Assink (jelle.assink@knmi.nl)
    Yochai Ben Horin (yochai.benhorin@gmail.com)

:copyright:
    2016-2020, Yochai Ben Horin, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
from obspy.core.inventory.util import Site
from obspy.core.inventory import Station
from obspy import UTCDateTime
from obspy.clients.nrl import NRL

from pysametadata.core.metadata import Metadata
# Instrument classes
from pysametadata.sensors.cea import MB200x
from pysametadata.dataloggers.kinemetrics import QuanterraQ330

t0 = UTCDateTime('2004-01-01T00:00:00')

IM_I0BR = Metadata(code='IM', name='I0BR', institute='Soreq',
                   description='NDC infra network')
IM_I0BR.set_network(code=IM_I0BR.code,
                    description='NDC infra network',
                    starttime=t0)
net = IM_I0BR.network

site_info = Site('Dimona INFRASONIC ARRAY',
                 description='Dimona INFRASONIC ARRAY',
                 town='Dimona', country='Israel')
# Station inventory
net.stations.append(Station('I0B1', 31.03008, 35.15785, 458.9, vault=1.0))
net.stations.append(Station('I0B2', 31.02313, 35.09582, 465.0, vault=1.0))
net.stations.append(Station('I0B3', 30.98000, 35.14368, 464.9, vault=1.0))
net.stations.append(Station('I0B4', 31.00883, 35.13393, 473.9, vault=1.0))

# Set up connection to Nominal Response Library (NRL)
nrl = NRL()
# Obtain response information for Obsidian digitiser to be used later
q330_keys = ['Quanterra', 'Q330SR', '1', '40', 'LINEAR AT ALL SPS']

print('Creating metadata objects ...')
for sta in net.stations:
    print(' - station [ {network}.{station} ]'.format(
        network=net.code, station=sta.code))
    IM_I0BR.set_station(sta, site_info, starttime=t0)

    # Channel content for the filtered output of the MB2000
    channel = IM_I0BR.set_channel('BDF', sta, sample_rate=40.0, starttime=t0)
    my_sensor = MB200x(channel, model='MB2000', mode='filtered')
    my_logger = QuanterraQ330(channel, V_max=20.0,
                              sens_freq=my_sensor.sens_freq, serial_number=1)
    channel.response = nrl.get_datalogger_response(q330_keys)
    channel.response.response_stages.pop(0)
    my_sensor.set_response(stage=1)
    IM_I0BR.calculate_sensitivity(channel)
    sta.channels.append(channel)

    # Channel content for the unfiltered output of the MB2000
    channel = IM_I0BR.set_channel('BDA', sta, sample_rate=40.0, starttime=t0)
    my_sensor = MB200x(channel, model='MB2000', mode='unfiltered')
    my_logger = QuanterraQ330(channel, V_max=20.0,
                              sens_freq=my_sensor.sens_freq, serial_number=1)
    channel.response = nrl.get_datalogger_response(q330_keys)
    channel.response.response_stages.pop(0)
    my_sensor.set_response(stage=1)
    IM_I0BR.calculate_sensitivity(channel)
    sta.channels.append(channel)

# Combine the network entries in one inventory
IM_I0BR.inventory.networks.append(IM_I0BR.network)

print('Plotting inventory ...')
IM_I0BR.inventory.plot(projection='local')

print('Writing out inventory ...')
IM_I0BR.write(plot_response=True)

del(IM_I0BR)
