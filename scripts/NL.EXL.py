#!/usr/bin/env python
# # -*- coding: utf-8 -*-
"""
Metadata for Exloo Infrasound Array (EXL)

.. module:: NL.EXL

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2016-2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
from obspy.core.inventory.util import Site
from obspy.core.inventory import Station
from obspy import UTCDateTime
from obspy.clients.nrl import NRL

from pysametadata.core.metadata import Metadata
# Instrument classes
from pysametadata.sensors.knmi import KNMImb
from pysametadata.dataloggers.knmi import KNMIpc
from pysametadata.dataloggers.kinemetrics import Obsidian

NL_EXL = Metadata(code='NL', name='EXL', institute='KNMI',
                  description='Royal Netherlands Meteorological Institute')
NL_EXL.set_network(code=NL_EXL.code,
                   description='Netherlands Seismic and Acoustic Network',
                   starttime=UTCDateTime('1993-01-01T00:00:00'))
net = NL_EXL.network

site_info = Site('Exloo Infrasound Array',
                 description='Exloo Infrasound Array',
                 town='Exloo', country='The Netherlands')
# Station inventory
net.stations.append(Station('EXL01', 52.908069, 6.865355, 6.0, vault=1.0))
net.stations.append(Station('EXL02', 52.906795, 6.865549, 7.0, vault=1.0))
net.stations.append(Station('EXL03', 52.908290, 6.864010, 6.0, vault=1.0))
net.stations.append(Station('EXL04', 52.906704, 6.865989, 4.0, vault=1.0))
net.stations.append(Station('EXL05', 52.908257, 6.867829, 6.0, vault=1.0))
net.stations.append(Station('EXL06', 52.907097, 6.865533, 6.0, vault=1.0))

# Initial deployment as ITS site (NL.EXL0?..BDF)
t0 = UTCDateTime('2005-01-01T00:00:00')
# End of first segment. Not sure when data stream actually stopped.
t1 = UTCDateTime('2013-01-01T00:00:00')
# Renovation EXL array using Kinemetrics datalogger (NL.EXL0?.00.HDF)
t2 = UTCDateTime('2017-07-20T18:00:00')    

# Set up connection to Nominal Response Library (NRL)
nrl = NRL()
# Obtain response information for Obsidian digitiser to be used later
obsidian_keys = ['Kinemetrics',
                 'Rock Family (Basalt, Granite, Dolomite, Obsidian)',
                 '1', '40', 'Non-causal', '100']

print('Creating metadata objects ...')
for sta in net.stations:
    print(' - station [ {network}.{station} ]'.format(
        network=net.code, station=sta.code))
    NL_EXL.set_station(sta, site_info, starttime=t0)

    # ---------------------------------------------------------------------
    # Initial deployment with KNMI-pc datalogger
    channel = NL_EXL.set_channel('BDF', sta,
                                 sample_rate=40.0, starttime=t0, endtime=t1)
    my_sensor = KNMImb(channel, model='500s', sens_freq=0.1, wnrs_diameter=10.0)
    my_logger = KNMIpc(channel, V_max=5.0, sens_freq=my_sensor.sens_freq)
    my_sensor.set_response(stage=1)
    my_logger.set_response(stage=2)
    NL_EXL.calculate_sensitivity(channel)
    sta.channels.append(channel)

    if sta.code != 'EXL05':
        # After renovation of EXL array
        channel = NL_EXL.set_channel('HDF', sta, location='00',
                                     sample_rate=100.0, starttime=t2)
        my_sensor = KNMImb(channel, model='500s', sens_freq=0.1, wnrs_diameter=10.0)
        my_logger = Obsidian(channel, V_max=20.0,
                             sens_freq=my_sensor.sens_freq, serial_number=1074)
        # channel.response = nrl.get_datalogger_response(obsidian_keys)
        # channel.response.response_stages.pop(0)
        my_sensor.set_response(stage=1)
        my_logger.set_response(stage=2)   # Comment this line when using NRL response
        NL_EXL.calculate_sensitivity(channel)
        sta.channels.append(channel)

# Combine the network entries in one inventory
NL_EXL.inventory.networks.append(NL_EXL.network)

print('Plotting inventory ...')
NL_EXL.inventory.plot(projection='local', resolution='f')

print('Writing out inventory ...')
NL_EXL.write(plot_response=True, freq_min=1e-4)

del(NL_EXL)
