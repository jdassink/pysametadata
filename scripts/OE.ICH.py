#!/usr/bin/env python
# # -*- coding: utf-8 -*-
"""
Metadata for Conrad Observatory Infrasound Array (ICH)

.. module:: OE.ICH

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
from obspy.core.inventory.util import Site
from obspy.core.inventory import Station
from obspy import UTCDateTime
from obspy.clients.nrl import NRL

from pysametadata.core.metadata import Metadata
# Instrument classes
from pysametadata.sensors.hyperion import IFS5000
from pysametadata.dataloggers.datalogger import Dummy

OE_ICH = Metadata(code='OE', name='ICH', institute='ZAMG',
                  description='ZAMG')
OE_ICH.set_network(code=OE_ICH.code,
                   description='ZAMG Seismic and Acoustic Network',
                   starttime=UTCDateTime('1993-01-01T00:00:00'))
net = OE_ICH.network

site_info = Site('Conrad Observatory Infrasound Array',
                 description='Conrad Observatory Infrasound Array',
                 town='Trafelberg', country='Austria')
# Station inventory
net.stations.append(Station('ICH1', 47.928038, 15.865356, 1124.0, vault=1.0))
net.stations.append(Station('ICH2', 47.929060, 15.858170, 1040.0, vault=1.0))
net.stations.append(Station('ICH3', 47.921463, 15.864420, 960.0, vault=1.0))
net.stations.append(Station('ICH4', 47.929971, 15.872115, 1126.0, vault=1.0))

# Original installation DBNI with KNMI-mb (NL.DBN0?..BDF)
t0 = UTCDateTime('2020-001T00:00:00')

print('Creating metadata objects ...')
for sta in net.stations:
    print(' - station [ {network}.{station} ]'.format(
        network=net.code, station=sta.code))
    OE_ICH.set_station(sta, site_info, starttime=t0)

    channel = OE_ICH.set_channel('BDF', sta, location='TR',
                                    sample_rate=20.0,
                                    starttime=t0, endtime=None)
    my_sensor = IFS5000(channel, wnrs_diameter=None, sens_freq=1.0)
    my_logger = Dummy(channel, gain=103571,
                      sens_freq=my_sensor.sens_freq)
    my_sensor.set_response(stage=1)
    my_logger.set_response(stage=2)
    OE_ICH.calculate_sensitivity(channel)
    sta.channels.append(channel)

# Combine the network entries in one inventory
OE_ICH.inventory.networks.append(OE_ICH.network)

print('Plotting inventory ...')
OE_ICH.inventory.plot(projection='local', resolution='f')

print('Writing out inventory ...')
OE_ICH.write(plot_response=True)

del(OE_ICH)
