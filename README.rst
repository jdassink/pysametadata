************
pysametadata
************

A **py** thon toolkit for **metadata** development, based on the Python infrastructure
developed by `ObsPy <https://github.com/obspy/obspy/wiki>`_,

The idea behind this package is to facilitate the preparation of station metadata using
Python scripts. The metadata is collected in ObsPy `Inventory` objects, which subsequently
can be written to community standard formats, such as `StationXML`. 
This procedure can lead to metadata that includes information to the response level.
It allows for the design of complex inventories, which consist of multi-disciplinary
sensor types, such as mobile seismo-acoustic array elements.

In addition, existing station metadata (i.e. in SeisComp3 format) can be read in and be
augmented, for example with response information.

The package includes response information of sensor types that are in use at the
R&D Seismology and Acoustics department of the Royal Netherlands Meteorological Institute.
This database of sensor types can readily be extended to custom sensors, in addition to
response information that is available through the `Nominal Response Library (NRL) 
<http://ds.iris.edu/NRL/>`_, as facilited through 
`ObsPy <https://docs.obspy.org/master/packages/obspy.clients.nrl.html>`_.


Installing this branch
----------------------

It is best if `obspy` and `pyproj` are first installed with
`conda`:

.. code-block:: console

    conda install obspy pyproj


Clone:

.. code-block:: console

    git clone git@gitlab.com:jdassink/pysametadata.git


Install:

.. code-block:: console

    cd pysametadata
    pip install -e .


Example Workflow
----------------

`Example scripts <https://gitlab.com/jdassink/pysametadata/-/blob/master/scripts>`_ 
and `interactive notebooks <https://gitlab.com/jdassink/pysametadata/-/blob/master/notebooks>`_ 
are included to get started.
